<?php

namespace Aplikace\Model;

class Ucebna
{
    public string $nazev;

    public int $kapacita;

    public function __construct(string $nazev, int $kapacita)
    {
        $this->nazev = $nazev;
        $this->kapacita = $kapacita;
    }

    public function __toString(): string
    {
        return 'Ucebna - Nazev: '.$this->nazev.', Kapacita: '.$this->kapacita;
    }
}