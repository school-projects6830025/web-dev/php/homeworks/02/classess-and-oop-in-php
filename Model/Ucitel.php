<?php

namespace Aplikace\Model;

require_once 'Osoba.php';

class Ucitel extends Osoba
{
    private array $predmety = [];

    public function __construct(string $jmeno, string $prijmeni, array $predmety)
    {
        parent::__construct($jmeno, $prijmeni);
        $this->predmety=$predmety;
    }

    public function __toString(): string
    {
        return 'Ucitel - '.parent::__toString().', Predmety: '.$this->getPredmetyStr();
    }

    public function getPredmetyStr(): string
    {
        return implode(', ', $this->predmety);
    }
}