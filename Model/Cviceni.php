<?php

namespace Aplikace\Model;

class Cviceni
{
    public string $nazev;

    public Ucitel $ucitel;

    public Ucebna $ucebna;

    private array $studenti;

    public function __construct(string $nazev, Ucitel $ucitel, Ucebna $ucebna, array $studenti)
    {
        $this->nazev = $nazev;
        $this->ucitel = $ucitel;
        $this->ucebna = $ucebna;
        $this->addStudent($studenti);
    }

    private function addStudent(array $studenti): void
    {
        foreach ($studenti as $student) {
            $this->studenti[] = $student;
        }
    }

    public function __toString(): string
    {
            return 'Cviceni - Nazev: '.$this->nazev.', '.$this->ucitel.', '.$this->ucebna.', Studenti: '.$this->getStudentiString();
    }

    /**
     * @return string[]
     */
    public function getStudenti(): array
    {
        return $this->studenti;
    }

    /**
     * @return string
     */
    public function getStudentiString(): string
    {
        return implode(', ', $this->studenti);
    }
}