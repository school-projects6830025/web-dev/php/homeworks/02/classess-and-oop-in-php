<?php

namespace Aplikace\Model;

abstract class Osoba {
    private string $jmeno;

    private string $prijmeni;

    protected string $id;

    public function __construct(string $jmeno, string $prijmeni) {
        $this->jmeno = $jmeno;
        $this->prijmeni = $prijmeni;
        $this->id = $this->generateId();
    }

    private function generateId():string {
        return uniqid();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return 'Osoba - Jmeno: '.$this->getJmeno().', Prijmeni: '.$this->getPrijmeni().', Id: '.$this->getId();
    }

    /**
     * @return mixed
     */
    public function getJmeno(): string
    {
        return $this->jmeno;
    }

    /**
     * @return mixed
     */
    public function getPrijmeni(): string
    {
        return $this->prijmeni;
    }
}