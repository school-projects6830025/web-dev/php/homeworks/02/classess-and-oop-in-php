<?php

namespace Aplikace\Model;

require_once 'Osoba.php';

class Student extends Osoba
{
    private int $rocnik = 1;

    public function __toString(): string
    {
        return 'Student - '.parent::__toString().', rocnik: '.$this->getRocnik();
    }

    public function getRocnik(): int
    {
        return $this->rocnik;
    }

    public function setRocnik(int $rocnik): void
    {
        $this->rocnik = $rocnik;
    }
}