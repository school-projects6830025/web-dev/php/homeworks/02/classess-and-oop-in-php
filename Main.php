<?php

namespace Aplikace;

require_once 'Model/Osoba.php';
require_once 'Model/Ucitel.php';
require_once 'Model/Student.php';
require_once 'Model/Ucebna.php';
require_once 'Model/Cviceni.php';

use Aplikace\Model\Osoba;
use Aplikace\Model\Ucitel;
use Aplikace\Model\Student;
use Aplikace\Model\Ucebna;
use Aplikace\Model\Cviceni;

function spustit(): void
{
    $honza = new Ucitel("Honza", "Pavlicek", array("Matematika", "Informatika", "Administrativa"));
    echo $honza . "\n";

    $matous = new Student("Matous", "Volenka");
    echo $matous . "\n";

    $studenti = [$matous];
    for ($tmp = 1; $tmp < 5; $tmp++) {
        $studenti[] = new Student("Jmeno" . $tmp, "Prijmeni" . $tmp);
    }

    $ucebna01 = new Ucebna("SB207", 30);
    echo $ucebna01 . "\n";

    $cviceni = new Cviceni("Matematika", $honza, $ucebna01, $studenti);
    echo $cviceni;
}

spustit();